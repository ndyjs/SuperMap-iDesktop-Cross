package com.supermap.desktop.process.parameter;

/**
 * @author XiaJT
 */
public interface ISingleSelectionParameter {
	Object getSelectedItem();

	void setSelectedItem(Object item);
}

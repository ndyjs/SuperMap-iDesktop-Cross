package com.supermap.desktop.process.enums;

/**
 * @author XiaJT
 */
public class ParameterType {
	public static final String COMBO_BOX = "ComboBox";
	public static final String ENUM = "Enum";
	public static final String TEXTFIELD = "TextField";
	public static final String DOUBLE = "Double";
	public static final String INT = "Int";
	public static final String RADIO_BUTTON = "RadioButton";
	public static final String CHECKBOX = "CheckBox";
	public static final String SPINNER = "Spinner";
	public static final String DATASOURCE = "Datasource";
	public static final String DATASOURCES = "Datasources";
	public static final String DATASET = "Dataset";
	public static final String DATASETS = "Datasets";
	public static final String FILES = "Files";
	public static final String FILE = "File";
	public static final String USER_DEFINE = "UserDefine";
	public static final String SAVE_DATASET = "SaveDataset";

}

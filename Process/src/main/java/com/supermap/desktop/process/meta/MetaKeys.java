package com.supermap.desktop.process.meta;

/**
 * Created by highsad on 2017/1/5.
 */
public class MetaKeys {
	public static String SPATIALINDEX = "SpatialIndex";
	public static String BUFFER = "Buffer";
	public static String IMPORT = "Import";
	public static String PROJECTION = "Projection";
}

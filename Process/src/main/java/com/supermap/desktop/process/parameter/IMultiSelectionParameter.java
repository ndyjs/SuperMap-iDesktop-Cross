package com.supermap.desktop.process.parameter;

/**
 * @author XiaJT
 */
public interface IMultiSelectionParameter {

	Object[] getSelectedItems();

	void setSelectedItems(Object[] selectedItems);


}

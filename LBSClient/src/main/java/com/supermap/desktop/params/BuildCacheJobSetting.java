package com.supermap.desktop.params;

/**
 * Created by xie on 2017/1/11.
 */
public class BuildCacheJobSetting {
    public FileInputDataSetting input = new FileInputDataSetting();
    public BuildCacheOutputSetting output = new BuildCacheOutputSetting();
    public BuildCacheDrawingSetting drawing = new BuildCacheDrawingSetting();
}

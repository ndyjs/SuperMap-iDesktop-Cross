package com.supermap.desktop.params;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

/**
 * Created by xie on 2017/1/11.
 */
public class BuildCacheOutputSetting {
    public String cacheName;
    public String cacheType;
}

package com.supermap.desktop.params;

/**
 * Created by xie on 2017/1/11.
 */
public class MongoDBOutputsetting extends BuildCacheOutputSetting{
    public String[] serverAdresses = new String[1];
    public String database;
    public String version;
}

package com.supermap.desktop.params;

/**
 * Created by xie on 2017/1/6.
 * 核密度分析类
 */
public class KernelDensityJobSetting {
    public FileInputDataSetting input = new FileInputDataSetting();
    public KernelDensityAnalystSetting analyst = new KernelDensityAnalystSetting();
}

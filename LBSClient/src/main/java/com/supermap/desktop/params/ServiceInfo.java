package com.supermap.desktop.params;

import java.util.ArrayList;

/**
 * Created by xie on 2017/1/11.
 */
public class ServiceInfo {
    public String targetDataPath;
    public ArrayList<IServerInfo> targetServiceInfos = new ArrayList<>();
}

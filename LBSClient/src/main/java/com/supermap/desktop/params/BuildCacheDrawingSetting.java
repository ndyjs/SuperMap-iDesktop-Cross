package com.supermap.desktop.params;

/**
 * Created by xie on 2017/1/11.
 */
public class BuildCacheDrawingSetting {
    public String imageType;
    public String bounds;
    public String level;
    public String xyIndex;
}
